'use strict';
require('dotenv').config();

const url = require('url');
const express = require('express');
const ObjectId = require('mongodb').ObjectID;
const MongoClient = require('mongodb').MongoClient;

const app = express();
const mongo = {
  client: null,
  db: null
};
let server = null;

app.use(express.urlencoded({extended: true}));

app.get('/', (request, response) => {
  response.sendFile('shortener.html', {root: 'public'});
});

app.get('/:id', async (request, response) => {
  try {
    const originalUrl = await unshortUrl(request.params.id);
    if (originalUrl) {
      return response.redirect(originalUrl);
    }
    return response.json({error: 'couldn\'t find URL'});
  } catch (error) {
    return response.json({error: 'couldn\'t unshort URL'});
  }
});

app.post('/api/shortener/new', async (request, response) => {
  const output = {
    originalUrl: request.body.url,
    shortUrl: request.headers.referer || url.format({
      protocol: request.protocol,
      host: request.headers.host,
      pathname: '/'
    })
  };
  try {
    output.originalUrl = (new url.URL(output.originalUrl)).toString();
  } catch (error) {
    return response.json({error: 'invalid URL'});
  }
  try {
    output.shortUrl += await shortUrl(output.originalUrl);
  } catch (error) {
    return response.json({error: 'couldn\'t short URL'});
  }
  response.json(output);
});

async function unshortUrl(id) {
  const url =
    await mongo.db.collection('urls').findOne({_id: new ObjectId(id)});
  if (url) {
    return url.originalUrl;
  }
  return null;
}

async function shortUrl(url) {
  const existingUrl =
    await mongo.db.collection('urls').findOne({originalUrl: url});
  if (existingUrl) {
    return existingUrl._id;
  }
  const result =
    await mongo.db.collection('urls').insertOne({originalUrl: url});
  return result.ops[0]._id;
}

async function clean() {
  await Promise.all([
    new Promise(resolve => server.close(resolve)),
    mongo.client.close()
  ]);
}

async function main() {
  mongo.client = await MongoClient.connect(process.env.MONGO_URI);
  mongo.db = mongo.client.db();
  server = app.listen(8080);
}

process.on('SIGINT', clean);
process.on('SIGTERM', clean);

main();
